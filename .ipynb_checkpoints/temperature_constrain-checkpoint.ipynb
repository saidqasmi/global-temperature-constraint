{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Update the Global warming projections using observations, following the [reference paper by Ribes, Qasmi, Gillet](https://doi.org/10.1126/sciadv.abc0671)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This notebook aims to update the Global Mean Surface Temperature (GMST) constraint. It requires the Kriging for Climate Change (KCC) package (available at: https://gitlab.com/saidqasmi/KCC)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(KCC)\n",
    "library(abind)\n",
    "# Set random number generator to ensure reproducibility\n",
    "set.seed(13)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set a reference period for observations\n",
    "ref_obs=1961:1990\n",
    "\n",
    "# Parameter nb resampling\n",
    "Nres = 1000\n",
    "sample_str = c(\"be\",paste0(\"nres\",1:Nres))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load the response to the external forcings (natural+anthropogenic) for all available CMIP6 models for the historical period and the three scenarios (SSP1-2.6, SSP2-4.5, SSP5-8.5)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "load(\"./data/X_fit_GSAT_CMIP6.Rdata\")\n",
    "year = as.numeric(dimnames(X_fit_26)$year)\n",
    "ny = length(year)\n",
    "\n",
    "Models_26 = dimnames(X_fit_26)$model\n",
    "Models_45 = dimnames(X_fit_45)$model\n",
    "Models_85 = dimnames(X_fit_85)$model\n",
    "Nmod_26 = length(Models_26)\n",
    "Nmod_45 = length(Models_45)\n",
    "Nmod_85 = length(Models_85)\n",
    "\n",
    "X_26_fit_anom_tmp = apply(X_fit_26[year %in% ref_obs,,], c(2,3), mean, na.rm=T)\n",
    "X_26_fit = sweep(X_fit_26, STATS=X_26_fit_anom_tmp, MARGIN=c(2,3))\n",
    "X_45_fit_anom_tmp = apply(X_fit_45[year %in% ref_obs,,], c(2,3), mean, na.rm=T)\n",
    "X_45_fit = sweep(X_fit_45, STATS=X_45_fit_anom_tmp, MARGIN=c(2,3))\n",
    "X_85_fit_anom_tmp = apply(X_fit_85[year %in% ref_obs,,], c(2,3), mean, na.rm=T)\n",
    "X_85_fit = sweep(X_fit_85, STATS=X_85_fit_anom_tmp, MARGIN=c(2,3))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Download the HadCRUT5 GMST observations directly from the Hadley Center server."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "Xo_raw = as.matrix(read.table(file=\"https://www.metoffice.gov.uk/hadobs/hadcrut5/data/current/analysis/diagnostics/HadCRUT.5.0.1.0.analysis.ensemble_series.global.annual.csv\",header=T, sep=\",\"))[,-c(1,2,3)]\n",
    "nmb = dim(Xo_raw)[2]\n",
    "\n",
    "# Extract data up to 2021\n",
    "year_obs = 1850:2021\n",
    "ny_obs = length(year_obs)\n",
    "Xog = array(data = cbind(Xo_raw[1:ny_obs,],apply(Xo_raw[1:ny_obs,],1,median)),\n",
    "            dim = c(ny_obs,nmb+1),\n",
    "            dimnames = list(year = year_obs,\n",
    "                            member = c(as.character(1:nmb),\"median\"))\n",
    "           )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We compute the new observational uncertainty"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Estimate uncertainty for internal variability (may take some time!)...\n",
      "\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "initial  value -624.223969 \n",
      "iter   2 value -627.449261\n",
      "iter   3 value -628.244722\n",
      "iter   4 value -628.509065\n",
      "iter   5 value -628.687491\n",
      "iter   6 value -629.809645\n",
      "iter   7 value -629.853958\n",
      "iter   8 value -629.857220\n",
      "iter   9 value -629.858411\n",
      "iter  10 value -629.858540\n",
      "iter  10 value -629.858543\n",
      "final  value -629.858543 \n",
      "converged\n"
     ]
    }
   ],
   "source": [
    "#  Calculate obs residuals\n",
    "Xg_mmm  = apply(X_45_fit[,\"all\",],1,mean,na.rm=T)\n",
    "Xgc_mmm = Xg_mmm - mean(Xg_mmm[year %in% ref_obs])\n",
    "Xogc    = Xog[as.character(year_obs),\"median\"] - mean(Xog[year_obs %in% ref_obs,\"median\"])\n",
    "Xog_res = Xogc - Xgc_mmm[as.character(year_obs)]\n",
    "# Fit the parameters of the MAR models on residuals\n",
    "message(\"Estimate uncertainty for internal variability (may take some time!)...\")\n",
    "theta_obs = estim_mar2_link(Xog_res)\n",
    "# Compute the associated covariance matrix\n",
    "Sigma_obs_iv = Sigma_mar2(theta_obs,ny_obs)\n",
    "# Add the measurement uncertainty contribution\n",
    "Sigma_obs = Sigma_obs_iv + var(t(Xog[as.character(year_obs),-201]))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compute the constrained projections..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_krig_26_list = prior2posterior(X_26_fit, Xog, Sigma_obs, Nres, centering_CX=T, ref_CX=1850:1900)\n",
    "X_krig_45_list = prior2posterior(X_45_fit, Xog, Sigma_obs, Nres, centering_CX=T, ref_CX=1850:1900)\n",
    "X_krig_85_list = prior2posterior(X_85_fit, Xog, Sigma_obs, Nres, centering_CX=T, ref_CX=1850:1900)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# X_unconstrained\n",
    "X_uncons_26 = mvgauss_to_Xarray(X_krig_26_list$uncons$mean,X_krig_26_list$uncons$var,Nres)\n",
    "X_uncons_45 = mvgauss_to_Xarray(X_krig_45_list$uncons$mean,X_krig_45_list$uncons$var,Nres)\n",
    "X_uncons_85 = mvgauss_to_Xarray(X_krig_85_list$uncons$mean,X_krig_85_list$uncons$var,Nres)\n",
    "# X_constrained\n",
    "X_cons_26 = mvgauss_to_Xarray(X_krig_26_list$cons$mean,X_krig_26_list$cons$var,Nres)\n",
    "X_cons_45 = mvgauss_to_Xarray(X_krig_45_list$cons$mean,X_krig_45_list$cons$var,Nres)\n",
    "X_cons_85 = mvgauss_to_Xarray(X_krig_85_list$cons$mean,X_krig_85_list$cons$var,Nres)\n",
    "# Put X_uncons and X_cons together\n",
    "l_tmp = c(dimnames(X_uncons_26)[1:3], list(constrain=c(\"uncons\",\"cons\")))\n",
    "X_krig_26 = abind(X_uncons_26, X_cons_26, along=4)\n",
    "X_krig_45 = abind(X_uncons_45, X_cons_45, along=4)\n",
    "X_krig_85 = abind(X_uncons_85, X_cons_85, along=4)\n",
    "dimnames(X_krig_26) = l_tmp\n",
    "dimnames(X_krig_45) = l_tmp\n",
    "dimnames(X_krig_85) = l_tmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ... and plot them !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Retrieve the results from Ribes et al. 2020\n",
      "\n"
     ]
    },
    {
     "ename": "ERROR",
     "evalue": "Error in text.default(0.5, 0.5, \"Second title\", cex = 2, font = 2): plot.new has not been called yet\n",
     "output_type": "error",
     "traceback": [
      "Error in text.default(0.5, 0.5, \"Second title\", cex = 2, font = 2): plot.new has not been called yet\nTraceback:\n",
      "1. text(0.5, 0.5, \"Second title\", cex = 2, font = 2)",
      "2. text.default(0.5, 0.5, \"Second title\", cex = 2, font = 2)"
     ]
    }
   ],
   "source": [
    "set_plot_dimensions <- function(width_choice, height_choice) {\n",
    "        options(repr.plot.width=width_choice, repr.plot.height=height_choice)\n",
    "        }\n",
    "\n",
    "source(\"routines/plot_cons.R\")\n",
    "ylim = c(-.5,7.7)\n",
    "ref_plot = 1850:1900\n",
    "message(\"Retrieve the results from Ribes et al. 2020\")\n",
    "set_plot_dimensions(16, 6)\n",
    "par(mfrow=c(1,3))\n",
    "plot_cons(X_krig_26, Xog, ref_plot=ref_plot, ny=ny, ylim=ylim, title=\"Historical+SSP1-2.6\")\n",
    "plot_cons(X_krig_45, Xog, ref_plot=ref_plot, ny=ny, ylim=ylim, title=\"Historical+SSP2-4.5\")\n",
    "plot_cons(X_krig_85, Xog, ref_plot=ref_plot, ny=ny, ylim=ylim, title=\"Historical+SSP5-8.5\")\n",
    "text(0.5,0.5,\"Second title\",cex=2,font=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quantification of the observed and projected warming"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Total forced warming over the 2012–2021 decade relative to the 1850–1900 preindustrial baseline\n",
      "\n",
      "   1.13, CI[1, 1.26]\n",
      "\n",
      "Total forced warming in 2021\n",
      "\n",
      "   1.22, CI[1.08, 1.37]\n",
      "\n",
      "-----------------\n",
      "\n",
      "Total forced warming in 2050 for SSP126\n",
      "\n",
      "   1.68, CI[1.42, 1.97]\n",
      "\n",
      "Total forced warming in 2050 for SSP245\n",
      "\n",
      "   1.92, CI[1.65, 2.23]\n",
      "\n",
      "Total forced warming in 2050 for SSP585\n",
      "\n",
      "   2.27, CI[1.89, 2.64]\n",
      "\n",
      "-----------------\n",
      "\n",
      "Total forced warming over the 2081-2100 period for SSP126\n",
      "\n",
      "   1.88, CI[1.43, 2.38]\n",
      "\n",
      "Total forced warming over the 2081-2100 period for SSP245\n",
      "\n",
      "   2.75, CI[2.2, 3.4]\n",
      "\n",
      "Total forced warming over the 2081-2100 period for SSP585\n",
      "\n",
      "   4.26, CI[3.26, 5.19]\n",
      "\n",
      "-----------------\n",
      "\n",
      "Total forced warming in 2100 for SSP126\n",
      "\n",
      "   1.89, CI[1.4, 2.44]\n",
      "\n",
      "Total forced warming in 2100 for SSP245\n",
      "\n",
      "   2.92, CI[2.29, 3.67]\n",
      "\n",
      "Total forced warming in 2100 for SSP585\n",
      "\n",
      "   4.76, CI[3.58, 5.84]\n",
      "\n"
     ]
    }
   ],
   "source": [
    "write_ci = function(v) {\n",
    "        message(\"   \",round(v[\"be\"],2),\", CI[\",round(quantile(v[-1],.05),2),\", \",round(quantile(v[-1],.95),2),\"]\")\n",
    "}\n",
    "\n",
    "\n",
    "# Calculation\n",
    "year_clim = 2012:2021\n",
    "year_mid = 2041:2060\n",
    "year_proj = 2081:2100\n",
    "\n",
    "Delta_obs = apply(X_krig_45[as.character(year_clim),,\"all\",\"cons\"],2,mean) - apply(X_krig_45[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_obs_today = X_krig_45[\"2021\",,\"all\",\"cons\"] - apply(X_krig_45[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_proj_26 = apply(X_krig_26[as.character(year_proj),,\"all\",\"cons\"],2,mean) - apply(X_krig_26[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_proj_45 = apply(X_krig_45[as.character(year_proj),,\"all\",\"cons\"],2,mean) - apply(X_krig_45[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_proj_85 = apply(X_krig_85[as.character(year_proj),,\"all\",\"cons\"],2,mean) - apply(X_krig_85[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2050_26 = X_krig_26[\"2050\",,\"all\",\"cons\"] - apply(X_krig_26[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2050_45 = X_krig_45[\"2050\",,\"all\",\"cons\"] - apply(X_krig_45[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2050_85 = X_krig_85[\"2050\",,\"all\",\"cons\"] - apply(X_krig_85[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2100_26 = X_krig_26[\"2100\",,\"all\",\"cons\"] - apply(X_krig_26[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2100_45 = X_krig_45[\"2100\",,\"all\",\"cons\"] - apply(X_krig_45[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "Delta_2100_85 = X_krig_85[\"2100\",,\"all\",\"cons\"] - apply(X_krig_85[year %in% 1850:1900,,\"all\",\"cons\"],2,mean)\n",
    "\n",
    "message(\"Total forced warming over the 2012–2021 decade relative to the 1850–1900 preindustrial baseline\")\n",
    "write_ci(Delta_obs)\n",
    "message(\"Total forced warming in 2021\")\n",
    "write_ci(Delta_obs_today)\n",
    "message(\"-----------------\")\n",
    "message(\"Total forced warming in 2050 for SSP126\")\n",
    "write_ci(Delta_2050_26)\n",
    "message(\"Total forced warming in 2050 for SSP245\")\n",
    "write_ci(Delta_2050_45)\n",
    "message(\"Total forced warming in 2050 for SSP585\")\n",
    "write_ci(Delta_2050_85)\n",
    "message(\"-----------------\")\n",
    "message(\"Total forced warming over the 2081-2100 period for SSP126\")\n",
    "write_ci(Delta_proj_26)\n",
    "message(\"Total forced warming over the 2081-2100 period for SSP245\")\n",
    "write_ci(Delta_proj_45)\n",
    "message(\"Total forced warming over the 2081-2100 period for SSP585\")\n",
    "write_ci(Delta_proj_85)\n",
    "message(\"-----------------\")\n",
    "message(\"Total forced warming in 2100 for SSP126\")\n",
    "write_ci(Delta_2100_26)\n",
    "message(\"Total forced warming in 2100 for SSP245\")\n",
    "write_ci(Delta_2100_45)\n",
    "message(\"Total forced warming in 2100 for SSP585\")\n",
    "write_ci(Delta_2100_85)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
